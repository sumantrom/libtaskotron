# -*- coding: utf-8 -*-
# Copyright 2009-2015, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from __future__ import absolute_import

from libtaskotron import config
from libtaskotron.logger import log
from libtaskotron import executor
from libtaskotron import file_utils
from libtaskotron import exceptions as exc
from libtaskotron.directives import exitcode_directive


class Overlord(object):
    """Overlord class encapsulates decision whether the task is run locally or remotely (in
    persistent or disposable client) and orchestrates the execution.

    :ivar dict arg_data: processed cli arguments with some extra runtime variables
    :ivar int exitcode: exit code of the task; if ``None``, it means :meth:`start` was not called
                        yet
    :ivar dict formula: parsed task formula
    """

    def __init__(self, arg_data):
        self.arg_data = arg_data
        self.exitcode = None

        # parse task formula
        self.formula = config.parse_yaml_from_file(arg_data['task'])
        if not self.formula:
            raise exc.TaskotronYamlError('Task formula file should not be empty: %s' %
                                         arg_data['task'])

    def _get_runner(self):
        """Decide the run mode and get appropriate runner (Executor or Minion).

        :returns: either :class:`.Executor` (local run mode) or :class:`.PersistentMinion` or
                  :class:`.DisposableMinion` (remote run mode)
        """
        # run locally or remotely
        run_remotely = False
        # when running remotely, run directly over ssh, instead of using libvirt
        persistent = False

        # decide whether to run locally or remotely
        runtask_mode = config.get_config().runtask_mode
        if runtask_mode == config.RuntaskModeName.LOCAL:
            run_remotely = False
        elif runtask_mode == config.RuntaskModeName.LIBVIRT:
            run_remotely = True
        else:
            assert False, 'This should never occur'

        if self.arg_data['local']:
            log.debug("Forcing local execution (option --local)")
            run_remotely = False

        elif self.arg_data['libvirt']:
            log.debug("Forcing remote execution (option --libvirt)")
            run_remotely = True
            persistent = False

        elif self.arg_data['ssh']:
            log.debug('Forcing remote execution (option --ssh)')
            run_remotely = True
            persistent = True

        log.debug('Execution mode: %s', 'remote' if run_remotely else 'local')

        if run_remotely:
            from libtaskotron import minion
            if persistent:
                return minion.PersistentMinion(self.formula, self.arg_data)
            else:
                return minion.DisposableMinion(self.formula, self.arg_data)
        else:
            return executor.Executor(self.formula, self.arg_data)

    def start(self):
        """Start the overlord, get runner and execute the task (either locally or remotely).
        """
        try:
            file_utils.makedirs(self.arg_data['artifactsdir'])
            log.info("Task artifacts will be saved in: %s", self.arg_data['artifactsdir'])
        except OSError:
            log.error("Can't create artifacts directory %s", self.arg_data['artifactsdir'])
            raise

        runner = self._get_runner()
        runner.execute()

        self.exitcode = 0
        if runner.exitcode == exitcode_directive.FAILURE:
            self.exitcode = runner.exitcode
