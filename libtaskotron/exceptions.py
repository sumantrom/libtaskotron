# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''This module contains custom Taskotron exceptions'''

from __future__ import absolute_import


class TaskotronError(Exception):
    '''Common ancestor for Taskotron related exceptions'''
    pass


class TaskotronValueError(ValueError, TaskotronError):
    '''Taskotron-specific :class:`ValueError`'''
    pass


class TaskotronConfigError(TaskotronError):
    '''All errors related to Taskotron config files'''
    pass


class TaskotronYamlError(TaskotronError):
    '''Error in YAML config file of the executed check'''
    pass


class TaskotronDirectiveError(TaskotronError):
    '''All errors related to Taskotron directives'''
    pass


class TaskotronRemoteError(TaskotronError):
    '''All network and remote-server related errors'''
    pass


class TaskotronRemoteTimeoutError(TaskotronRemoteError):
    '''A remote-server did not send any output in a given timeout'''
    pass


class TaskotronRemoteProcessError(TaskotronRemoteError):
    '''A process/command executed remotely returned a non-zero exit code, crashed or failed in a
    different way'''
    pass


class TaskotronNotImplementedError(TaskotronError, NotImplementedError):
    '''NotImplementedError for Taskotron classes, methods and functions'''


class TaskotronImportError(TaskotronError):
    '''All issues with Extensions'''

    def __init__(self, e):
        msg = str(e.message)
        if 'libtaskotron' in msg:
            msg += "\nHint: Make sure that all required sub-packages are installed."
        super(TaskotronError, self).__init__(msg)


class TaskotronImageError(TaskotronError):
    '''All generic image related errors'''
    pass


class TaskotronImageNotFoundError(TaskotronImageError):
    '''Requested image not found error'''
    pass


class TaskotronPermissionError(TaskotronError):
    '''Insufficient permissions or privileges'''
    pass
