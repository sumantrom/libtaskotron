# -*- coding: utf-8 -*-
# Copyright 2009-2015, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from __future__ import absolute_import
import tempfile
import os.path
import imp
import copy
import collections
import pipes

from libtaskotron import taskformula
from libtaskotron import logger
from libtaskotron import python_utils
from libtaskotron import config
from libtaskotron.logger import log
import libtaskotron.exceptions as exc
from libtaskotron.directives import exitcode_directive

try:
    from libtaskotron.ext.fedora import rpm_utils
except ImportError, e:
    raise exc.TaskotronImportError(e)


class Executor(object):
    '''This class serves the purpose of actual task execution. It is instantiated by
    :class:`.Overlord`, :class:`.PersistentMinion` or :class:`.DisposableMinion`.

    :ivar dict formula: parsed task formula
    :ivar dict arg_data: processed cli arguments with some extra runtime variables
    :ivar str workdir: path to working directory; a temporary one is created during execution if
                       ``None``
    :ivar dict working_data: variables exported from formula directives, to be injected when
                             referenced using ``${variable}``
    :ivar int exitcode: exit code set by ``exitcode`` directive; it is ``None`` if ``exitcode``
                        directive was not used
    '''

    def __init__(self, formula, arg_data, workdir=None):
        self.formula = formula
        self.arg_data = arg_data
        self.working_data = {}
        self.directives = {}
        self.workdir = workdir
        self.exitcode = None

    def execute(self):
        '''Execute the task (consists of preparing the task and running it).'''

        filelog_path = os.path.join(self.arg_data['artifactsdir'], 'taskotron.log')
        logger.add_filehandler(filelog_path=filelog_path, remove_mem_handler=True)

        self._prepare_task()
        self._run()

    def _prepare_task(self):
        '''Prepare the environment for running a task. If there are packages, requirements or
        groups specified in the task formula, this method tries to install them (in case of
        production profile) or checks whether they are installed (in case of developer profile).
        '''
        rpms = self.formula.get('environment', {}).get('rpm', [])
        profile = config.get_config().profile

        if not rpm_utils.is_installed(rpms):
            if profile == config.ProfileName.PRODUCTION or self.arg_data['local']:
                rpm_utils.install(rpms)
            else:
                raise exc.TaskotronError('Some packages are not installed. Please run '
                                         '"dnf install %s" to install all required packages.'
                                         % " ".join([pipes.quote(rpm) for rpm in rpms]))

    def _run(self):
        self._validate_input()

        if not self.workdir:  # create temporary workdir if needed
            self.workdir = tempfile.mkdtemp(prefix="task-",
                                            dir=config.get_config().tmpdir)
        log.debug("Current workdir: %s", self.workdir)
        self.arg_data['workdir'] = self.workdir
        self.arg_data['checkname'] = self.formula['name']
        self.arg_data['namespace'] = self.formula.get('namespace', 'scratch')

        # override variable values
        for var, val in self.arg_data['override'].items():
            log.debug("Overriding variable %s, new value: %s", var, val)
            self.arg_data[var] = eval(val, {}, {})

        self._do_actions()

    def _validate_input(self):
        if 'input' not in self.formula:
            return

        if not isinstance(self.formula['input'], collections.Mapping):
            raise exc.TaskotronYamlError("Input yaml should contain correct 'input'"
                                         "section (a mapping). Yours was: %s" %
                                         type(self.formula['input']))

        required_args = self.formula['input'].get('args', None)

        if not python_utils.iterable(required_args):
            raise exc.TaskotronYamlError("Input yaml should contain correct 'args' "
                                         "section (an iterable). Yours was: %s" %
                                         type(required_args))

        for arg in required_args:
            if arg not in self.arg_data:
                raise exc.TaskotronYamlError("Required input arg '%s' "
                                             "was not defined" % arg)

    def _do_actions(self):
        '''Sequentially run all actions for a task. An 'action' is a single step
        under the ``actions:`` key. An example action looks like::

          - name: download rpms from koji
            koji:
              action: download
              koji_build: $koji_build
              arch: $arch
        '''
        if 'actions' not in self.formula or not self.formula['actions']:
            raise exc.TaskotronYamlError("At least one task should be specified"
                                         " in input formula")

        for action in self.formula['actions']:
            self._do_single_action(action)

    def _do_single_action(self, action):
        '''Execute a single action from the task. See :meth:`do_actions` to see
        how an action looks like.

        :param dict action: An action specification parsed from the task formula
        '''
        directive_name = self._extract_directive_from_action(action)

        rendered_action = self._render_action(action)

        log.debug("Executing directive: %s", directive_name)
        self._load_directive(directive_name)

        directive_object = self.directives[directive_name]
        directive_classname = directive_object.directive_class

        directive_callable = getattr(directive_object, directive_classname)()

        output = directive_callable.process(rendered_action[directive_name],
                                            self.arg_data)

        if 'export' in action:
            self.working_data[action['export']] = output
            log.debug("Variable ${%s} was exported with value:\n%s" %
                      (action['export'], output))

        # unable to use isinstance() due to dynamically loaded directives
        if directive_classname == exitcode_directive.directive_class:
            # set exitcode if was None or overwrite previous SUCCESS with FAILURE (not vice versa)
            if not self.exitcode or output != exitcode_directive.SUCCESS:
                log.info("Setting exitcode to %s" % output)
                self.exitcode = output

    def _extract_directive_from_action(self, action):
        for key in action:
            if key not in ['name', 'export']:
                return key
        raise exc.TaskotronYamlError('no directive found in action %s' % str(action))

    def _render_action(self, action):
        '''Take an action and replace all included variables with actual values
        from :attr:`arg_data` and :attr:`working_data`. See :meth:`do_actions`
        to see how an action looks like.

        :param dict action: An action specification parsed from the task formula
        :return: a rendered action
        :rtype: dict
        '''
        # copy the input so that we don't disrupt what we're processing
        rendered_action = copy.deepcopy(action)

        variables = copy.deepcopy(self.arg_data)
        variables.update(self.working_data)
        taskformula.replace_vars_in_action(rendered_action, variables)

        return rendered_action

    def _load_directive(self, directive_name, directive_dir=None):
        # look in default path if nothing is specified
        if not directive_dir:
            directive_dir = os.path.join(os.path.dirname(__file__),
                                         'directives')

        real_name = "%s_directive" % directive_name
        directive_file = os.path.join(directive_dir, '%s.py' % real_name)

        if not os.path.exists(directive_file):
            raise exc.TaskotronDirectiveError("Directive %s not found in directory %s" %
                                              (directive_name, directive_dir))

        try:
            loaded_directive = imp.load_source(real_name, directive_file)
        except ImportError, e:
            raise exc.TaskotronImportError(e)

        self.directives[directive_name] = loaded_directive

    def _validate_env(self):
        # TODO: implement this
        raise NotImplementedError("Environment validation is not yet implemented")
