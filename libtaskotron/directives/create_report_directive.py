# -*- coding: utf-8 -*-
# Copyright 2009-2016, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from __future__ import absolute_import


DOCUMENTATION = """
module: create_report_directive
short_description: generate report of task results.
description: |
  Generate report of task results in HTML format to given path.

parameters:
  results:
    required: true
    description: |
      A string containing task results in the ResultYAML format. You must use either ``results``
      or ``file`` parameter (exactly one).
    type: str
  file:
    required: true
    description: |
      A path to a file containing the task results in the ResultYAML format. You must use either
      ``results`` or ``file`` parameter (exactly one).
    type: str
  path:
    required: false
    description: |
      A path and a filename of the generated HTML report. You should always use relative
      paths, and ``${artifactsdir}`` is considered to be the base directory. All missing
      directories will be created automatically. Example: ``reports/overview.html``
    type: str
    default: report.html
  template:
    required: false
    description: |
      A path to a template to be used to generate a report. The path is supposed to be relative to
      the task directory. By default, a standard libtaskotron report template will be used.
      Example: ``report.j2``
    type: str
raises: |
  * :class:`.TaskotronDirectiveError`: if both ``results`` and ``file`` parameters are used; or if
    their contents is not in a valid YAML format; or if ``path`` cannot be created.
version_added: 0.4
"""

EXAMPLES = """
These two actions first run ``run_rpmlint.py`` and export its YAML output to
``${rpmlint_output}``, and then feed this YAML output to the ``create_report``
directive::

    - name: run rpmlint on downloaded rpms
      python:
          file: run_rpmlint.py
          callable: run
          workdir: ${workdir}
      export: rpmlint_output

    - name: make html report of the results
      create_report:
          results: ${rpmlint_output}

This example shows using the ``file`` parameter. The ``randomtest`` produces a ``testoutput.yml``
file containing results in the YAML format, that the ``create_report`` directive then uses::

    - name: run randomtest on downloaded rpms
      shell:
          - bash ./randomtest.sh --outfile=${workdir}/testoutput.yml

    - name: make html report of the results
      create_report:
          file: ${workdir}/testoutput.yml
"""

import os

import jinja2

from libtaskotron.directives import BaseDirective

from libtaskotron import check
from libtaskotron import file_utils
from libtaskotron.exceptions import TaskotronDirectiveError, TaskotronValueError
from libtaskotron.logger import log


directive_class = 'CreateReportDirective'


class CreateReportDirective(BaseDirective):

    def process(self, params, arg_data):

        if 'file' in params and 'results' in params:
            raise TaskotronDirectiveError("Either `file` or `results` can be used, not both.")

        try:
            if params.get('file', None):
                with open(params['file']) as resultfile:
                    params['results'] = resultfile.read()

            check_details = check.import_YAML(params['results'])
            log.debug("YAML output parsed OK.")
        except (TaskotronValueError, IOError) as e:
            raise TaskotronDirectiveError("Failed to load results: %s" % e.message)

        log.debug('Generating report of %s results...', len(check_details))

        results = []
        for detail in check_details:
            results.append({
                'checkname': detail.checkname or arg_data['checkname'],
                'outcome': detail.outcome,
                'note': detail.note or '---',
                'item': detail.item,
                'type': detail.report_type,
                'artifact': os.path.basename(detail.artifact) if detail.artifact else None,
            })

        if 'path' in params:
            report_fname = os.path.join(arg_data['artifactsdir'], params['path'])
        else:
            report_fname = os.path.join(arg_data['artifactsdir'], 'report.html')

        if 'template' in params:
            template_fname = os.path.join(os.path.dirname(arg_data['task']),
                                          params['template'])
        else:
            template_fname = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                          '../report_templates/html.j2')

        try:
            file_utils.makedirs(os.path.dirname(report_fname))
        except OSError as e:
            raise TaskotronDirectiveError(e)

        with open(template_fname) as f_template:
            with open(report_fname, 'w') as f_report:
                template = jinja2.Template(f_template.read())
                report = template.render(results=results,
                                         artifactsdir=arg_data['artifactsdir'])
                f_report.write(report)

        log.info('Report generated in: %s', os.path.abspath(report_fname))
