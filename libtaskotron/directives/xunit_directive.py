# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from __future__ import absolute_import

import xunitparser

from libtaskotron.directives import BaseDirective
from libtaskotron.exceptions import TaskotronDirectiveError
from libtaskotron.check import CheckDetail, export_YAML

DOCUMENTATION = """
module: xunit_directive
short_description: parse XML xUnit file
description: |
  Parse test ouput in XML xUnit format and generate :doc:`ResultYAML </resultyaml>` for further use
  (i.e. directly usable in :doc:`resultsdb directive <resultsdb_directive_module>`).

  .. note::
    The input xUnit file is set as an artifact in the output ResultYAML result, therefore it's
    recommended that you place it inside ``${artifactsdir}``.
parameters:
  file:
    required: true
    description: xUnit XML file path to be parsed, either absolute or relative to task directory
    type: str
  aggregation:
    required: false
    description: |
      type of result aggregation: ``allpass`` will aggregate testcases into a single result with
      outcome ``PASSED`` if all testcases passed, otherwise ``FAILED``. ``none`` means no
      aggregation and all testcases will be reported as separate results.
    type: str
    default: allpass
    choices: [allpass, none]
returns: |
  check result(s) in the ResultYAML format
raises: |
  * :class:`.TaskotronDirectiveError`: if mandatory parameters are missing or incorrect parameter
    values were provided
version_added: 0.4.14
"""

EXAMPLES = """
This example parses xUnit XML file located in ``${artifactsdir}``, generates ResultYAML from it and
exports it to a variable which is then passed to ``resultsdb`` directive::

  - name: parse xunit output
    xunit:
        file: ${artifactsdir}/xunit_output.xml
    export: resultyaml

  - name: report results to resultsdb
    resultsdb:
        results: ${resultyaml}
"""

directive_class = 'XunitDirective'


class XunitDirective(BaseDirective):

    aggregations = ['none', 'allpass']

    def process(self, params, arg_data):
        if 'file' not in params:
            raise TaskotronDirectiveError('No file provided.')
        xunitfile = params['file']

        aggregation = params.get('aggregation', 'allpass')

        if not arg_data['item'] or not arg_data['type']:
            raise TaskotronDirectiveError('Item and its type has to be provided.')

        if aggregation not in self.aggregations:
            raise TaskotronDirectiveError(
                "Aggregation '%s' is not one of: %s" % (aggregation, ', '.join(self.aggregations)))

        with open(xunitfile) as xmlfile:
            testsuite, testresult = xunitparser.parse(xmlfile)

        if aggregation == 'none':
            details = []
            for tc in testsuite:
                outcome = 'PASSED' if tc.good else 'FAILED'
                details.append(CheckDetail(item=arg_data['item'],
                                           checkname="%s.%s" % (arg_data['checkname'],
                                                                tc.methodname),
                                           report_type=arg_data['type'],
                                           outcome=outcome,
                                           artifact=xunitfile))
            return export_YAML(details)

        elif aggregation == 'allpass':
            passed = len([tc for tc in testsuite if tc.good])
            failed = len([tc for tc in testsuite if not tc.good])
            final_outcome = 'PASSED' if failed == 0 else 'FAILED'
            note = CheckDetail.create_multi_item_summary(['PASSED'] * passed + ['FAILED'] * failed)

            return export_YAML(CheckDetail(item=arg_data['item'],
                                           note=note,
                                           report_type=arg_data['type'],
                                           outcome=final_outcome,
                                           artifact=xunitfile))

        else:
            assert False, "This should never happen, aggregation is %r" % aggregation
