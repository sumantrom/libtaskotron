.. _dist-git-tasks:

===============
Dist-git tasks
===============

How to have a taskotron task run from dist-git
==============================================

.. note::
  The feature is new and rough edges are expected. If you run into issues, please let us know so
  we can fix them.

Once the task is written (see :ref:`writing-tasks-for-taskotron`), it can be pushed into
`dist-git <http://pkgs.fedoraproject.org/cgit/rpms-checks>`_ and be run on different actions.
Currently, only action supported is 'run the task on build completion in koji'. This is going to
change in the near future and actions like 'push to dist-git' or 'upload to look-aside cache' will
be supported as well. Taskotron determines on what action to run a particular task according to
the ``input`` section in the formula, in this case on koji build completion.

.. code-block:: yaml

  ---
  name: mytask
  desc: do something stupendous
  maintainer: somefasuser

  input:
      args:
          - arch
          - koji_build

Note that the formula yaml file must be named ``runtask.yml``. Taskotron tasks dist-git works the
same way as the dist-git for Fedora packages but it is located in a different
`namespace <http://pkgs.fedoraproject.org/cgit/rpms-checks>`_. Each tasks must be in a separate
directory in the repo. Branches work the same way as rpms dist-git as well, so tasks for Fedora 24
will be in the ``f24`` branch and so on. Example of the directory structure of the repo follows:

::

  |-- sanity_check
  |   |-- sanity_check.py
  |   \-- runtask.yml
  \-- services_test
      |-- services_test.sh
      \-- runtask.yml
