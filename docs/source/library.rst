=====================
libtaskotron API docs
=====================

This isn't final API documentation, more of an example of how we can do API docs.

Once more of our code is using sphinx-style docs, we can start finalizing the
format for this page.

Note that the docs here are for libtaskotron library functions and don't
include directives or task yaml details.


Core modules
============

arch_utils
----------

.. automodule:: libtaskotron.arch_utils
   :members:

buildbot_utils
--------------

.. automodule:: libtaskotron.buildbot_utils
  :members:

check
-----

.. automodule:: libtaskotron.check
   :members:

config
------

.. automodule:: libtaskotron.config
   :members:

config_defaults
---------------

.. automodule:: libtaskotron.config_defaults
   :members:

exceptions
----------

.. automodule:: libtaskotron.exceptions
   :members:

executor
--------

.. automodule:: libtaskotron.executor
   :members:

file_utils
----------

.. automodule:: libtaskotron.file_utils
   :members:

logger
------

.. automodule:: libtaskotron.logger
   :members:

main
----

.. automodule:: libtaskotron.main
   :members:

minion
------

.. automodule:: libtaskotron.minion
  :members:

os_utils
--------

.. automodule:: libtaskotron.os_utils
   :members:

overlord
--------

.. automodule:: libtaskotron.overlord
   :members:

python_utils
------------

.. automodule:: libtaskotron.python_utils
   :members:

remote_exec
-----------

.. automodule:: libtaskotron.remote_exec
   :members:

taskformula
-----------

.. automodule:: libtaskotron.taskformula
   :members:


Disposable minion modules
=========================

vm
--

.. automodule:: libtaskotron.ext.disposable.vm
   :members:


Fedora modules
==============

bodhi_utils
-----------

.. automodule:: libtaskotron.ext.fedora.bodhi_utils
   :members:

koji_utils
----------

.. automodule:: libtaskotron.ext.fedora.koji_utils
   :members:

rpm_utils
---------

.. automodule:: libtaskotron.ext.fedora.rpm_utils
   :members:

yumrepoinfo
-----------

.. automodule:: libtaskotron.ext.fedora.yumrepoinfo
   :members:

