# This is a list of pypi packages to be installed into virtualenv. Alternatively,
# you can install these as RPMs instead of pypi packages. Use
# `rpmspec -q --requires libtaskotron.spec` and
# `rpmspec -q --buildrequires libtaskotron.spec` to get a list of RPM dependencies.
#
# A note for maintainers: Please keep this list in sync with libtaskotron.spec.
# You should require exact versions here (in order to define an exact working
# environment), and require a minimal version in the spec file (in order to allow
# the libraries to have some minor version bumps during the Fedora release
# lifecycle). If the Fedora libraries span multiple versions across releases,
# you can define a dependency range here instead of an exact version.

configparser == 3.5.0b2
dingus == 0.3.4
Jinja2 == 2.8
munch == 2.0.2
mock == 2.0.0
paramiko == 1.15.1
pytest == 2.7.3
pytest-cov == 2.2.1
python-fedora == 0.8.0
progressbar == 2.3
requests == 2.7.0
PyYAML == 3.11
resultsdb_api == 1.2.2
Sphinx == 1.2.3
xunitparser == 1.3.3
