# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

import pytest
from dingus import Dingus

from libtaskotron import executor
from libtaskotron import config
from libtaskotron import file_utils
from libtaskotron import minion
from libtaskotron import overlord


@pytest.mark.usefixtures('setup')
class TestOverlord():

    @pytest.fixture
    def setup(self, monkeypatch):
        self.stub_local = Dingus()
        self.stub_remote = Dingus()
        self.stub_remote_persistent = Dingus()
        monkeypatch.setattr(config, 'parse_yaml_from_file', Dingus())
        monkeypatch.setattr(file_utils, 'makedirs', Dingus())

        monkeypatch.setattr(executor, 'Executor', self.stub_local)
        monkeypatch.setattr(minion, 'DisposableMinion', self.stub_remote)
        monkeypatch.setattr(minion, 'PersistentMinion', self.stub_remote_persistent)

        self.stub_get_config = Dingus()
        self.stub_config_local = Dingus()
        self.stub_config_local.runtask_mode = config.RuntaskModeName.LOCAL
        self.stub_config_remote = Dingus()
        self.stub_config_remote.runtask_mode = config.RuntaskModeName.LIBVIRT

        self.empty_args = {'local': None, 'libvirt': None, 'ssh': None, 'ssh_privkey': None,
                           'uuid': '', 'task': '', 'artifactsdir': '/dir'}

    def test_local_no_cli(self, monkeypatch):
        '''run locally with local runmode in config and no override from cli'''
        ref_params = self.empty_args.copy()
        self.stub_get_config.return_value = self.stub_config_local
        monkeypatch.setattr(config, 'get_config', self.stub_get_config)

        test_runner = overlord.Overlord(ref_params)

        test_runner.start()
        assert len(self.stub_local.calls()) == 1
        assert len(self.stub_remote.calls()) == 0
        assert len(self.stub_remote_persistent.calls()) == 0

    def test_remote_no_cli(self, monkeypatch):
        '''run remotely with remote runmode in config and no override from cli'''
        ref_params = self.empty_args.copy()
        self.stub_get_config.return_value = self.stub_config_remote
        monkeypatch.setattr(config, 'get_config', self.stub_get_config)

        test_runner = overlord.Overlord(ref_params)

        test_runner.start()
        assert len(self.stub_local.calls()) == 0
        assert len(self.stub_remote.calls()) == 1
        assert len(self.stub_remote_persistent.calls()) == 0

    def test_remote_override_local(self, monkeypatch):
        '''run locally with remote runmode in config and local override from cli'''
        ref_params = self.empty_args.copy()
        ref_params.update({'local': True})
        self.stub_get_config.return_value = self.stub_config_remote
        monkeypatch.setattr(config, 'get_config', self.stub_get_config)

        test_runner = overlord.Overlord(ref_params)

        test_runner.start()
        assert len(self.stub_local.calls()) == 1
        assert len(self.stub_remote.calls()) == 0
        assert len(self.stub_remote_persistent.calls()) == 0

    def test_local_override_ssh(self, monkeypatch):
        '''run remotely with local runmode in config and ssh override from cli'''
        ref_params = self.empty_args.copy()
        ref_params.update({'ssh': 'david@HAL9000'})
        self.stub_get_config.return_value = self.stub_config_local
        monkeypatch.setattr(config, 'get_config', self.stub_get_config)

        test_runner = overlord.Overlord(ref_params)

        test_runner.start()
        assert len(self.stub_local.calls()) == 0
        assert len(self.stub_remote.calls()) == 0
        assert len(self.stub_remote_persistent.calls()) == 1

    def test_local_override_libvirt(self, monkeypatch):
        '''run remotely with local runmode in config and libvirt override from cli'''
        ref_params = self.empty_args.copy()
        ref_params.update({'libvirt': True})
        self.stub_get_config.return_value = self.stub_config_local
        monkeypatch.setattr(config, 'get_config', self.stub_get_config)

        test_runner = overlord.Overlord(ref_params)

        test_runner.start()
        assert len(self.stub_local.calls()) == 0
        assert len(self.stub_remote.calls()) == 1
        assert len(self.stub_remote_persistent.calls()) == 0

    def test_executor_used(self, monkeypatch):
        monkeypatch.setattr(config, 'parse_yaml_from_file', Dingus())
        monkeypatch.setattr(file_utils, 'makedirs', Dingus())

        ref_params = {'local': True, 'ssh': None, 'ssh_privkey': None, 'uuid': '',
                         'task': '', 'artifactsdir': '/dir'}

        stub_local_runner = Dingus()
        monkeypatch.setattr(executor, 'Executor', stub_local_runner)

        test_runner = overlord.Overlord(ref_params)

        test_runner.start()

        # check that LocalRunner was instantiated
        assert len(stub_local_runner.calls()) == 1
