# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from libtaskotron.directives import exitcode_directive
from libtaskotron.exceptions import TaskotronDirectiveError, TaskotronValueError
from libtaskotron import overlord
from libtaskotron import config
from libtaskotron import file_utils
from dingus import Dingus
import pytest


@pytest.mark.usefixtures("setup")
class TestReturncode(object):

    @pytest.fixture
    def setup(self):
        self.test_arg_data = {}
        self.test_params = {}
        self.test_yaml = """
results:
  - item: Hello World P
    outcome: PASSED
  - item: Hello World I
    outcome: INFO
  - item: Hello World F
    outcome: FAILED
  - item: Hello World P
    outcome: PASSED
"""

    def test_result_last(self):
        self.test_params['result_last'] = self.test_yaml
        exitcode = exitcode_directive.ExitcodeDirective().process(self.test_params,
                                                                  self.test_arg_data)
        assert exitcode == exitcode_directive.SUCCESS

    def test_result_worst(self):
        self.test_params['result_worst'] = self.test_yaml
        exitcode = exitcode_directive.ExitcodeDirective().process(self.test_params,
                                                                  self.test_arg_data)
        assert exitcode == exitcode_directive.FAILURE

    def test_bad_input(self):
        with pytest.raises(TaskotronDirectiveError):
            exitcode_directive.ExitcodeDirective().process(self.test_params,
                                                           self.test_arg_data)

    def test_bad_yaml(self):
        self.test_yaml = "key:value\nkey2: value"
        self.test_params['result_worst'] = self.test_yaml
        with pytest.raises(TaskotronValueError):
            exitcode_directive.ExitcodeDirective().process(self.test_params,
                                                           self.test_arg_data)

    def test_empty_result_worst(self):
        self.test_yaml = "{results:[]}"
        self.test_params['result_worst'] = self.test_yaml
        exitcode = exitcode_directive.ExitcodeDirective().process(self.test_params,
                                                                  self.test_arg_data)
        assert exitcode == exitcode_directive.SUCCESS

    def test_empty_result_last(self):
        self.test_yaml = "{results:[]}"
        self.test_params['result_last'] = self.test_yaml
        exitcode = exitcode_directive.ExitcodeDirective().process(self.test_params,
                                                                  self.test_arg_data)
        assert exitcode == exitcode_directive.SUCCESS


@pytest.mark.usefixtures("setup")
class TestRunnerExitcode(object):

    @pytest.fixture
    def setup(self, monkeypatch):
        arg_data = {'local': None,
                    'libvirt': None,
                    'ssh': None,
                    'ssh_privkey': None,
                    'uuid': '',
                    'task': '',
                    'no_destroy': None}
        arg_data['artifactsdir'] = '/dir'

        monkeypatch.setattr(config, 'parse_yaml_from_file', Dingus())
        monkeypatch.setattr(file_utils, 'makedirs', Dingus())
        self.test_runner = overlord.Overlord(arg_data)

        self.stub_runner = Dingus()
        self.stub_runner.exitcode = exitcode_directive.FAILURE

    def test_has_exitcode(self):
        assert hasattr(self.test_runner, 'exitcode')

    def test_local_propagate(self, monkeypatch):
        stub_local = Dingus()
        stub_local.return_value = self.stub_runner
        monkeypatch.setattr(self.test_runner, '_get_runner', stub_local)
        self.test_runner.start()
        assert self.test_runner.exitcode == exitcode_directive.FAILURE

    def test_remote_propagate(self, monkeypatch):
        stub_remote = Dingus()
        stub_remote.return_value = self.stub_runner
        monkeypatch.setattr(self.test_runner, '_get_runner', stub_remote)
        self.test_runner.start()
        assert self.test_runner.exitcode == exitcode_directive.FAILURE
