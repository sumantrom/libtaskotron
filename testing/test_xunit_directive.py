# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from libtaskotron.directives import xunit_directive
from libtaskotron.exceptions import TaskotronDirectiveError
from libtaskotron.check import import_YAML

import mock
import pytest
import xunitparser

OUTPUT = 'I shall not use Czech in source code'

XML = """<?xml version="1.0"?>
<testsuite errors="0" failures="0" name="pytest" skips="0" tests="3" time="17.131">
<testcase classname="test.TestClass" file="test.py" line="30" name="test1" time="2.76759004593">
<system-out></system-out></testcase>
<testcase classname="test.TestClass" file="test.py" line="34" name="test2" time="2.67303919792">
<system-out></system-out></testcase>
<testcase classname="test.TestClass" file="test.py" line="52" name="test3" time="11.6780951023">
<system-out></system-out></testcase></testsuite>"""


class TestCheckParams(object):

    def setup(self):
        self.params = {'file': 'json.xml'}
        self.arg_data = {'item': 'some_item', 'type': 'some_type'}
        self.directive = xunit_directive.XunitDirective()

    @pytest.fixture
    def mock_parse(self, monkeypatch):
        stub_parse = mock.MagicMock(return_value=([], True))
        monkeypatch.setattr(xunitparser, 'parse', stub_parse)

    @pytest.fixture
    def mock_open(self, monkeypatch, read_data=None):
        monkeypatch.setattr(xunit_directive, 'open', mock.mock_open(read_data=read_data),
                            raising=False)

    def test_file_present(self, monkeypatch, mock_parse, mock_open):
        monkeypatch.setattr(xunit_directive, 'export_YAML', mock.Mock(return_value=OUTPUT))

        output = self.directive.process(self.params, self.arg_data)
        assert output == OUTPUT

    def test_file_not_present(self, mock_parse, mock_open):
        self.params = {'fila': 'json.xml'}

        with pytest.raises(TaskotronDirectiveError):
            self.directive.process(self.params, self.arg_data)

    def test_item_not_present(self, mock_parse, mock_open):
        self.arg_data['item'] = None

        with pytest.raises(TaskotronDirectiveError):
            self.directive.process(self.params, self.arg_data)

    def test_type_not_present(self, mock_parse, mock_open):
        self.arg_data['type'] = None

        with pytest.raises(TaskotronDirectiveError):
            self.directive.process(self.params, self.arg_data)

    def test_file_does_not_exist(self, mock_parse):
        with pytest.raises(IOError):
            self.directive.process(self.params, self.arg_data)

    def test_aggregation_unknown(self, mock_parse):
        self.params['aggregation'] = 'allfail'

        with pytest.raises(TaskotronDirectiveError):
            self.directive.process(self.params, self.arg_data)

    def test_aggregation_none(self, monkeypatch):
        self.params['aggregation'] = 'none'
        self.arg_data['checkname'] = 'mycheck'
        self.mock_open(monkeypatch, read_data=XML)

        output = self.directive.process(self.params, self.arg_data)
        assert len(import_YAML(output)) == 3

    def test_aggregation_allpass(self, monkeypatch):
        self.params['aggregation'] = 'allpass'
        self.mock_open(monkeypatch, read_data=XML)

        output = self.directive.process(self.params, self.arg_data)
        assert len(import_YAML(output)) == 1

    def test_aggregation_allpass_default(self, monkeypatch):
        self.mock_open(monkeypatch, read_data=XML)

        output = self.directive.process(self.params, self.arg_data)
        assert len(import_YAML(output)) == 1
