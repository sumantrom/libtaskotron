# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

import pytest
from dingus import Dingus

from libtaskotron import executor
from libtaskotron import main
from libtaskotron import config
from libtaskotron.ext.fedora import rpm_utils
from libtaskotron.directives import exitcode_directive
from libtaskotron import exceptions as exc


class TestExecutorInputVerify():
    def test_yamlexecutor_valid_input(self):
        ref_cmd = '-i foo-1.2-3.fc99 -t koji_build -a x86_64 footask.yml'
        ref_arg_data = {'input': {'args': ['koji_build', 'arch']}, 'uuid': ''}

        test_parser = main.get_argparser()
        test_input_args = vars(test_parser.parse_args(ref_cmd.split()))
        test_args = main.process_args(test_input_args)

        test_executor = executor.Executor(ref_arg_data, test_args)

        test_executor._validate_input()

    def test_yamlexecutor_missing_input(self):
        ref_cmd = '-i foo-1.2-3.fc99 -t koji_build footask.yml'
        ref_inputspec = {}

        test_parser = main.get_argparser()
        test_input_args = vars(test_parser.parse_args(ref_cmd.split()))
        test_args = main.process_args(test_input_args)

        test_executor = executor.Executor(ref_inputspec, test_args)

        test_executor._validate_input()

    def test_yamlexecutor_fails_empty_input(self):
        ref_cmd = '-i foo-1.2-3.fc99 -t koji_build footask.yml'
        ref_inputspec = {'input': None}

        test_parser = main.get_argparser()
        test_input_args = vars(test_parser.parse_args(ref_cmd.split()))
        test_args = main.process_args(test_input_args)

        test_executor = executor.Executor(ref_inputspec, test_args)

        with pytest.raises(exc.TaskotronYamlError):
            test_executor._validate_input()

    def test_yamlexecutor_fails_empty_arg(self):
        ref_cmd = '-i foo-1.2-3.fc99 -t koji_build footask.yml'
        ref_inputspec = {'input': {'args': None}}

        test_parser = main.get_argparser()
        test_input_args = vars(test_parser.parse_args(ref_cmd.split()))
        test_args = main.process_args(test_input_args)

        test_executor = executor.Executor(ref_inputspec, test_args)

        with pytest.raises(exc.TaskotronYamlError):
            test_executor._validate_input()

    def test_yamlexecutor_fails_missing_arg(self):
        ref_cmd = '-i foo-1.2-3.fc99 -t koji_build footask.yml'
        ref_inputspec = {'input': 'other_item'}

        test_parser = main.get_argparser()
        test_input_args = vars(test_parser.parse_args(ref_cmd.split()))
        test_args = main.process_args(test_input_args)

        test_executor = executor.Executor(ref_inputspec, test_args)

        with pytest.raises(exc.TaskotronYamlError):
            test_executor._validate_input()

    def test_yamlexecutor_fails_undefined_arg(self):
        ref_cmd = '-i foo-1.2-3.fc99 -t koji_build footask.yml'
        ref_inputspec = {'input': {'args': 'nonexistent_input_arg'}}

        test_parser = main.get_argparser()
        test_input_args = vars(test_parser.parse_args(ref_cmd.split()))
        test_args = main.process_args(test_input_args)

        test_executor = executor.Executor(ref_inputspec, test_args)

        with pytest.raises(exc.TaskotronYamlError):
            test_executor._validate_input()

    def test_yamlexecutor_fails_input_is_string(self):
        ref_cmd = '-i foo-1.2-3.fc99 -t koji_build footask.yml'
        ref_inputspec = {'input': {'args'}}

        test_parser = main.get_argparser()
        test_input_args = vars(test_parser.parse_args(ref_cmd.split()))
        test_args = main.process_args(test_input_args)

        test_executor = executor.Executor(ref_inputspec, test_args)

        with pytest.raises(exc.TaskotronYamlError):
            test_executor._validate_input()


class TestExecutorSetup():
    def test_trivial_creation(self, tmpdir):
        ref_formula = {'preparation': {'koji': 'download nvr'},
                       'input': {'args': ['koji_build', 'arch']},
                       'post': {'shell': 'clean'},
                       'execution': {'python': 'run_rpmlint.py'},
                       'dependencies': ['rpmlint', 'libtaskbot']}
        ref_params = {'uuid': ''}

        test_executor = executor.Executor(ref_formula, ref_params, tmpdir)

        assert test_executor.formula == ref_formula


class TestExecutorSingleAction():

    def setup_method(self, method):
        self.ref_formula = {}
        self.ref_params = {'uuid': ''}

        self.ref_yaml_passed = """
results:
  - item: Hello World P
    outcome: PASSED
"""
        self.ref_yaml_failed = """
results:
  - item: Hello World P
    outcome: FAILED
"""

    def test_executor_extract_directive_name(self):
        ref_directivename = 'dummy'
        ref_action = {'name': 'test action',
                      ref_directivename: {'result': 'pass',
                                          'msg': 'dummy message'}}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)

        test_directivename = test_executor._extract_directive_from_action(
            ref_action)

        assert test_directivename == ref_directivename

    def test_executor_single_dummy_action_pass(self):
        ref_action = {'dummy': {'result': 'pass'}}
        ref_action.update({'name': 'Dummy Action'})

        test_executor = executor.Executor(self.ref_formula, self.ref_params)

        test_executor._do_single_action(ref_action)

    def test_executor_single_dummy_action_fail(self):
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'fail'}}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)

        with pytest.raises(exc.TaskotronDirectiveError):
            test_executor._do_single_action(ref_action)

    def test_executor_do_single_dummy_action_pass_export(self):
        ref_exportname = 'dummy_output'
        ref_output = 'this is example output'
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'pass', 'msg': ref_output},
                      'export': '%s' % ref_exportname}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)

        test_executor._do_single_action(ref_action)

        assert test_executor.working_data[ref_exportname] == ref_output

    def test_executor_do_single_dummy_action_pass_export_rendered_msg(self):
        ref_exportname = 'dummy_output'
        ref_message = 'This is a variable message'
        ref_messagename = 'variable_messsage'
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'pass',
                                'msg': '${%s}' % ref_messagename},
                      'export': '%s' % ref_exportname}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)
        test_executor.working_data[ref_messagename] = ref_message

        test_executor._do_single_action(ref_action)

        assert test_executor.working_data[ref_exportname] == ref_message

    def test_executor_do_single_dummy_action_exitcode_None(self):
        ref_action = {'name': 'Return exitcode', 'exitcode': {'result_last': self.ref_yaml_passed}}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)

        test_executor._do_single_action(ref_action)

        assert test_executor.exitcode == exitcode_directive.SUCCESS

    def test_executor_do_single_dummy_action_exitcode_success(self):
        ref_action = {'name': 'Return exitcode', 'exitcode': {'result_last': self.ref_yaml_passed}}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)

        test_executor._do_single_action(ref_action)

        assert test_executor.exitcode == exitcode_directive.SUCCESS

    def test_executor_do_single_dummy_action_exitcode_failure(self):
        ref_action = {'name': 'Return exitcode', 'exitcode': {'result_last': self.ref_yaml_failed}}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)

        test_executor._do_single_action(ref_action)

        assert test_executor.exitcode == exitcode_directive.FAILURE

    def test_executor_do_single_dummy_action_exitcode_failure_overwrite_success(self):
        ref_action = {'name': 'Return exitcode', 'exitcode': {'result_last': self.ref_yaml_failed}}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)
        test_executor.exitcode = exitcode_directive.SUCCESS
        test_executor._do_single_action(ref_action)

        assert test_executor.exitcode == exitcode_directive.FAILURE

    def test_executor_do_single_dummy_action_exitcode_success_not_overwrite_failure(self):
        ref_action = {'name': 'Return exitcode', 'exitcode': {'result_last': self.ref_yaml_passed}}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)
        test_executor.exitcode = exitcode_directive.FAILURE
        test_executor._do_single_action(ref_action)

        assert test_executor.exitcode == exitcode_directive.FAILURE


class TestExecutorDoActions():

    def setup_method(self, method):
        self.ref_formula = {}
        self.ref_params = {'uuid': ''}

    def test_executor_do_single_dummy_pass(self):
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'pass'}}
        self.ref_formula = {'actions': [ref_action]}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)

        test_executor._do_actions()

    def test_executor_do_single_dummy_fail(self):
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'fail'}}
        self.ref_formula = {'actions': [ref_action]}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)

        with pytest.raises(exc.TaskotronDirectiveError):
            test_executor._do_actions()

    def test_executor_do_multiple_dummy_fail(self):
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'fail'}}
        self.ref_formula = {'actions': [ref_action, ref_action]}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)

        with pytest.raises(exc.TaskotronDirectiveError):
            test_executor._do_actions()

    def test_executor_do_multiple_dummy_pass_export_rendered_msg(self):
        ref_exportname = 'dummy_output'
        ref_message = 'This is a variable message'
        ref_messagename = 'variable_messsage'
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'pass',
                                'msg': '${%s}' % ref_messagename},
                      'export': '%s' % ref_exportname}

        self.ref_formula = {'actions': [ref_action, ref_action]}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)
        test_executor.working_data[ref_messagename] = ref_message

        test_executor._do_actions()

        assert test_executor.working_data[ref_exportname] == ref_message

    def test_executor_fails_missing_action(self):
        self.ref_formula = {'actions': None}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)

        with pytest.raises(exc.TaskotronYamlError):
            test_executor._do_actions()

    def test_executor_fails_missing_task(self):
        self.ref_formula = {}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)

        with pytest.raises(exc.TaskotronYamlError):
            test_executor._do_actions()


class TestExecutorRenderAction():

    def setup_method(self, method):
        self.ref_formula = {}
        self.ref_params = {'uuid': ''}

    def test_executor_render_single_variable(self):
        ref_message = 'This is a variable message'
        ref_messagename = 'variable_messsage'
        ref_action = {'name': 'test dummy action', 'dummy':
                      {'result': 'pass', 'msg': '${%s}' % ref_messagename}}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)
        test_executor.working_data[ref_messagename] = ref_message

        test_rendered_action = test_executor._render_action(ref_action)

        assert test_rendered_action['dummy']['msg'] == ref_message

    def test_executor_render_list_variable(self):
        ref_var = ['foo', 'bar']
        ref_varname = 'testvar'
        ref_action = {'name': 'test dummy action', 'dummy':
                      {'result': 'pass', 'msg': '${%s}' % ref_varname}}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)
        test_executor.working_data[ref_varname] = ref_var

        test_rendered_action = test_executor._render_action(ref_action)

        assert test_rendered_action['dummy']['msg'] == ref_var

    def test_executor_render_multi_variable(self):
        ref_var1 = 'foo'
        ref_var1name = 'testvar1'
        ref_var2 = 'bar'
        ref_var2name = 'testvar2'
        ref_action = {'name': 'test dummy action', 'dummy':
                      {'result': 'pass', 'msg': '--foo ${%s} --bar ${%s}' %
                       (ref_var1name, ref_var2name)}}

        test_executor = executor.Executor(self.ref_formula, self.ref_params)
        test_executor.working_data[ref_var1name] = ref_var1
        test_executor.working_data[ref_var2name] = ref_var2

        test_rendered_action = test_executor._render_action(ref_action)

        assert (test_rendered_action['dummy']['msg'] == '--foo %s --bar %s' %
                (ref_var1, ref_var2))


@pytest.mark.usefixtures('setup')
class TestExecutorCheckOverride():

    @pytest.fixture
    def setup(self):
        self.ref_input = {'arch': ['x86_64'],
                          'type': 'koji_build',
                          'item': 'foo-1.2-3.fc99',
                          'uuid': '20150220_134110_955656',
                          'ssh': None,
                          'local': False,
                          'override': ["workdir='/some/dir/'"],
                          'task': 'task.yaml'}

    def test_override_existing(self):
        ref_params = main.process_args(self.ref_input)

        test_executor = executor.Executor(Dingus(), ref_params)
        test_executor._do_actions = lambda: None
        test_executor._run()

        assert test_executor.arg_data['workdir'] == "/some/dir/"

    def test_override_nonexisting(self):
        self.ref_input['override'] = ["friendship='is magic'"]
        ref_params = main.process_args(self.ref_input)

        test_executor = executor.Executor(Dingus(), ref_params)
        test_executor._do_actions = lambda: None
        test_executor._run()

        assert test_executor.arg_data['friendship'] == "is magic"


@pytest.mark.usefixtures('setup')
class TestExecutorPrepareTask():

    @pytest.fixture
    def setup(self, monkeypatch):
        self.install_stub = Dingus()
        self.is_installed_stub = Dingus()
        self.stub_config = Dingus()
        self.stub_get_config = Dingus()

        self.stub_get_config.return_value = self.stub_config
        monkeypatch.setattr(rpm_utils, 'install', self.install_stub)
        monkeypatch.setattr(rpm_utils, 'is_installed', self.is_installed_stub)
        monkeypatch.setattr(config, 'get_config', self.stub_get_config)

        self.test_executor = executor.Executor({}, {'local': False, 'ssh': None, 'uuid': ''})

    def test_devel_profile_installed(self):
        self.is_installed_stub.return_value = True
        self.stub_config.profile = 'devel'

        self.test_executor._prepare_task()

        assert len(self.install_stub.calls()) == 0 and len(self.is_installed_stub.calls()) == 1

    def test_devel_profile_not_installed(self):
        self.is_installed_stub.return_value = False
        self.stub_config.profile = 'devel'

        with pytest.raises(exc.TaskotronError):
            self.test_executor._prepare_task()

        assert len(self.install_stub.calls()) == 0 and len(self.is_installed_stub.calls()) == 1

    def test_prod_profile(self):
        self.is_installed_stub.return_value = False
        self.stub_config.profile = 'production'

        self.test_executor._prepare_task()

        assert len(self.install_stub.calls()) == 1 and len(self.is_installed_stub.calls()) == 1
