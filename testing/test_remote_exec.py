# -*- coding: utf-8 -*-
# Copyright 2009-2015, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Unit tests for libtaskotron/remote_exec.py'''

import errno
import socket
import os

import pytest
import paramiko
from munch import Munch
import mock

from libtaskotron import remote_exec
from libtaskotron.exceptions import TaskotronRemoteError, TaskotronRemoteProcessError


STUB_DIR = 'dirname'
STUB_FILE = 'filename'
STUB_NONEXISTING_FILE = ''
# stub for a file that is too large (no space left on a device)
STUB_ENOSPC = 1
# file mode
STUB_REF_MODE = None
STUB_NONEXISTING_HOST1 = 'ip1'
STUB_NONEXISTING_HOST2 = 'ip2'
STUB_NONEXISTING_USER = 'foo'
STUB_NONWRITABLE_FILE = 'permissiondenied'
STUB_CMD_TRUE = 0
STUB_CMD_FALSE = 1


class StubChannel(object):
    def __init__(self, cmd):
        self.cmd = cmd
        self._status_ready = False
        self._timeout = None

    def exit_status_ready(self):
        ret = self._status_ready
        self._status_ready = True
        return ret

    def recv_ready(self):
        return True

    def recv(self, count):
        return 'command output'

    def recv_stderr_ready(self):
        return True

    def recv_stderr(self, count):
        return 'command error'

    def recv_exit_status(self):
        return self.cmd

    def settimeout(self, timeout):
        self._timeout = timeout


class StubSFTPClient(object):
    def __init__(self, *args, **kwargs):
        pass

    def close(self, *args, **kwargs):
        pass

    def open(self, path, mode):
        if path == STUB_NONWRITABLE_FILE:
            raise IOError

    def put(self, localpath, remotepath):
        if remotepath == STUB_ENOSPC:
            raise IOError

        attrs = paramiko.SFTPAttributes()
        attrs.st_mode = STUB_REF_MODE

        return attrs

    def get(self, remotepath, localpath):
        if localpath == STUB_ENOSPC:
            raise IOError

        return None

    def lstat(self, path):
        attrs = paramiko.SFTPAttributes()

        if path == STUB_DIR:
            attrs.st_mode = 16893  # mode of a directory
        elif path == STUB_FILE:
            attrs.st_mode = 33204  # mode of a file
        elif path == STUB_NONEXISTING_FILE:
            raise IOError(errno.ENOENT, 'No such file')

        return attrs

    def listdir_attr(self, path):
        if not path == STUB_DIR:
            raise IOError

    def mkdir(self, *args, **kwargs):
        pass

    def get_channel(self):
        return StubChannel('')


class StubSSHClient(object):
    def __init__(self, *args, **kwargs):
        self._sftp = None

    def set_missing_host_key_policy(self, *args, **kwargs):
        pass

    def connect(self, hostname, port, username, key_filename):
        if hostname == STUB_NONEXISTING_HOST1:
            raise paramiko.SSHException
        elif hostname == STUB_NONEXISTING_HOST2:
            raise socket.error
        if username == STUB_NONEXISTING_USER:
            raise paramiko.AuthenticationException

    def close(self, *args, **kwargs):
        pass

    def open_sftp(self, *args, **kwargs):
        if self._sftp is None:
            self._sftp = StubSFTPClient()

        return self._sftp

    def exec_command(self, cmd, **kwargs):
        ch = Munch(channel=StubChannel(cmd), read=lambda: '')

        return ch, ch, ch


@pytest.mark.usefixtures('setup')
class TestRemoteExec(object):

    @pytest.fixture
    def setup(self, monkeypatch):
        '''Run this before every test invocation'''
        self.ref_host_ip = '192.168.122.22'
        paramiko.SSHClient = StubSSHClient
        self.remote = remote_exec.ParamikoWrapper(self.ref_host_ip, 22, 'root', 'keyfilename')
        self.remote.connect()
        mock_mkdir = mock.Mock()
        monkeypatch.setattr(os, 'mkdir', mock_mkdir)

    def teardown_method(self, method):
        self.remote.close()

    def test_remote_isdir(self):
        assert self.remote._remote_isdir(STUB_DIR) is True

    def test_remote_isdir_not(self):
        assert self.remote._remote_isdir(STUB_FILE) is False

    def test_remote_isdir_not_exist(self):
        assert self.remote._remote_isdir(STUB_NONEXISTING_FILE) is False

    def test_remote_file_exists(self):
        assert self.remote._remote_file_exists(STUB_FILE) is True

    def test_remote_file_doesnt_exists(self):
        assert self.remote._remote_file_exists(STUB_NONEXISTING_FILE) is False

    def test_get_file(self):
        assert self.remote.get_file(STUB_FILE, STUB_FILE) is None

    def test_get_file_ioerror(self):
        with pytest.raises(TaskotronRemoteError):
            self.remote.get_file(STUB_FILE, STUB_ENOSPC)

    def test_put_file(self):
        attrs = self.remote.put_file(STUB_FILE, STUB_FILE)
        assert attrs.st_mode == STUB_REF_MODE

    def test_put_file_ioerror(self):
        with pytest.raises(TaskotronRemoteError):
            self.remote.put_file(STUB_FILE, STUB_ENOSPC)

    def test_put_file_overwrite(self):
        attrs = self.remote.put_file(STUB_FILE, STUB_FILE, overwrite=True)
        assert attrs.st_mode == STUB_REF_MODE

    def test_put_file_dont_overwrite(self):
        assert self.remote.put_file(STUB_FILE, STUB_FILE, overwrite=False) is None

    def test_connect_nonexisting_host1(self):
        remote = remote_exec.ParamikoWrapper(STUB_NONEXISTING_HOST1, 22, 'root', 'keyfilename')
        with pytest.raises(TaskotronRemoteError):
            remote.connect()

    def test_connect_nonexisting_host2(self):
        remote = remote_exec.ParamikoWrapper(STUB_NONEXISTING_HOST2, 22, 'root', 'keyfilename')
        with pytest.raises(TaskotronRemoteError):
            remote.connect()

    def test_connect_nonexisting_user(self):
        remote = remote_exec.ParamikoWrapper(self.ref_host_ip, 22, STUB_NONEXISTING_USER,
                                             'keyfilename')
        with pytest.raises(TaskotronRemoteError):
            remote.connect()

    def test_write_file_not_accessible_file(self):
        ref_data = 'foobar'
        with pytest.raises(TaskotronRemoteError):
            self.remote.write_file(STUB_NONWRITABLE_FILE, ref_data)

    def test_write_file_dont_overwrite(self):
        ref_data = 'foobar'
        assert self.remote.write_file(STUB_FILE, ref_data, overwrite=False) is None

    def test_put_dir_not_dir(self):
        with pytest.raises(TaskotronRemoteError):
            self.remote.put_dir(STUB_FILE, STUB_DIR)

    def test_put_dir_not_dir_dont_overwrite(self):
        assert self.remote.put_dir(STUB_DIR, STUB_DIR, overwrite=False) is None

    def test_get_dir_not_dir(self):
        with pytest.raises(TaskotronRemoteError):
            self.remote.get_dir(STUB_FILE, STUB_DIR)

    def test_cmd_success(self):
        assert self.remote.cmd(STUB_CMD_TRUE) == STUB_CMD_TRUE

    def test_cmd_failure(self):
        with pytest.raises(TaskotronRemoteError):
            self.remote.cmd(STUB_CMD_FALSE)

    def test_install_pkgs(self):
        self.remote.cmd = mock.Mock()
        self.remote.install_pkgs(['libtaskotron', 'rpmlint'])
        assert self.remote.cmd.call_count == 1
        cmd = self.remote.cmd.call_args[0][0]
        words = ['dnf', 'install', 'libtaskotron', 'rpmlint']
        assert all([word in cmd for word in words])

    def test_install_pkgs_try_twice(self):
        '''If the first execution fails, modify dnf options and try again'''
        err = TaskotronRemoteProcessError()
        self.remote.cmd = mock.Mock(side_effect=[err, 0])
        pkgs = ['libtaskotron', 'rpmlint']
        self.remote.install_pkgs(pkgs)

        assert self.remote.cmd.call_count == 2
        cmd1 = self.remote.cmd.call_args_list[0][0][0]
        words1 = ['dnf', 'install', '--assumeyes', '--cacheonly', '--best'] + pkgs
        assert all([word in cmd1 for word in words1])
        cmd2 = self.remote.cmd.call_args_list[1][0][0]
        words2 = ['dnf', 'install', '--assumeyes', '--refresh', '--best'] + pkgs
        not_words2 = ['--cacheonly']
        assert all([word in cmd2 for word in words2])
        assert all([word not in cmd2 for word in not_words2])

    def test_install_pkgs_try_three_times(self):
        '''If the first two executions fail, modify dnf options and try again'''
        err = TaskotronRemoteProcessError()
        self.remote.cmd = mock.Mock(side_effect=[err, err, 0])
        pkgs = ['libtaskotron', 'rpmlint']
        self.remote.install_pkgs(pkgs)

        assert self.remote.cmd.call_count == 3
        # first two cmdline args were tested in test_install_pkgs_try_twice()
        cmd3 = self.remote.cmd.call_args_list[2][0][0]
        words3 = ['dnf', 'install', '--assumeyes'] + pkgs
        not_words3 = ['--cacheonly', '--refresh', '--best']
        assert all([word in cmd3 for word in words3])
        assert all([word not in cmd3 for word in not_words3])

    def test_install_pkgs_fail(self):
        self.remote.cmd = mock.Mock(side_effect=TaskotronRemoteProcessError())
        with pytest.raises(TaskotronRemoteProcessError):
            self.remote.install_pkgs(['libtaskotron', 'rpmlint'])
        assert self.remote.cmd.call_count == 3
