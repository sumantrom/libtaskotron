# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from libtaskotron.directives import mash_directive
import os
import shutil
import pytest
import rpmfluff


class TestMashDirective(object):
    @pytest.fixture(autouse=True)
    def setup_method(self, tmpdir):
        NVRAs = [
            ["wine", "0.1", "3", ["i686", "x86_64"]],
            ["foo", "0.2", "2", ["i686"]],
        ]

        #store current dir
        startdir = os.getcwd()

        workdir = tmpdir.strpath
        self.rpmdir = os.path.join(workdir, "rpmdir")

        print self.rpmdir

        os.chdir(workdir)
        os.mkdir(self.rpmdir)

        packages = [rpmfluff.SimpleRpmBuild(*NVRA) for NVRA in NVRAs]

        for package in packages:
            package.make()

        rpms = [p.get_built_rpm("i686") for p in packages]

        for rpm in rpms:
            shutil.copy(rpm, self.rpmdir)

        self.rpms = [os.path.basename(rpm) for rpm in rpms]

        #jump back
        os.chdir(startdir)

    def test_mash_arch_i686(self):
        params = {
            'rpmdir': self.rpmdir,
            'arch': ['i386']
        }
        directive = mash_directive.MashDirective()
        before = os.listdir(self.rpmdir)
        directive.process(params, {})
        after = os.listdir(self.rpmdir)

        assert self.rpms[0] in before and self.rpms[0] in after and \
               self.rpms[1] in before and self.rpms[1] in after


    def test_mash_arch_x86_64(self):
        params = {
            'rpmdir': self.rpmdir,
            'arch': ['x86_64']
        }
        directive = mash_directive.MashDirective()
        before = os.listdir(self.rpmdir)
        directive.process(params, {})
        after = os.listdir(self.rpmdir)

        assert self.rpms[0] in before and self.rpms[0] in after and \
               self.rpms[1] in before and self.rpms[1] not in after

    def test_outdir_is_created(self, tmpdir):
        outdir_path = tmpdir.join('somewhere/along/the/highway').strpath
        params = {
            'rpmdir': self.rpmdir,
            'outdir': outdir_path,
            'arch': ['i386']
        }

        directive = mash_directive.MashDirective()
        ret_path = directive.process(params, {})

        assert ret_path == outdir_path
        assert os.path.isdir(ret_path)


    def test_repodata_in_rpmdir(self):
        params = {
            'rpmdir': self.rpmdir,
            'arch': ['x86_64']
        }
        directive = mash_directive.MashDirective()
        directive.process(params, {})
        after = os.listdir(self.rpmdir)

        assert 'repodata' in after

    def test_repodata_in_outdir(self, tmpdir):
        outdir = tmpdir.strpath
        params = {
            'rpmdir': self.rpmdir,
            'outdir': outdir,
            'arch': ['x86_64']
        }
        directive = mash_directive.MashDirective()
        directive.process(params, {})
        after = os.listdir(self.rpmdir)

        assert 'repodata' not in after and 'repodata' in os.listdir(outdir)
