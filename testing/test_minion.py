# -*- coding: utf-8 -*-
# Copyright 2016, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Unit tests for libtaskotron/minion.py'''

import argparse
import mock
import pytest
import shlex

from libtaskotron import minion
from libtaskotron import main
import libtaskotron.exceptions as exc


@pytest.mark.usefixtures('setup')
class TestBaseMinion(object):

    @pytest.fixture
    def setup(self):
        self.ref_arg_data = {'artifactsdir': '/somedir/',
                             'task': '/path/mytask.yaml',
                             'ssh_privkey': None,
                             '_orig_args': {'item': 'foo',
                                            'type': 'koji_tag'}}
        self.bm = minion.BaseMinion(None, self.ref_arg_data)
        self.bm.taskdir = '/minion/path/'
        self.mock_ssh = mock.Mock()
        self.bm.ssh = self.mock_ssh

    def test_arg_data_exlude_nothing_extra(self):
        '''If there's some option defined in arg_data_exclude that is not present in current
        argparser, it's either a typo or some leftover.'''
        args_names = [action.dest for action in main.get_argparser()._actions]
        for exclude in minion.BaseMinion.arg_data_exclude:
            assert exclude in args_names

    def test_forward_args_allowed_actions(self):
        '''The only allowed actions for non-ignored argparser options are 'store', 'store_true',
        and 'append', otherwise it breaks libtaskotron.python_utils.reverse_argparse()'''
        for action in main.get_argparser()._actions:
            if action.dest in minion.BaseMinion.arg_data_exclude:
                continue
            # also allow HelpAction, that gets added automatically
            assert isinstance(action, (argparse._StoreAction, argparse._StoreTrueAction,
                                       argparse._AppendAction, argparse._HelpAction))

    def test_run_cmd_called(self):
        '''This ensures ssh.cmd() was called with a single argument. Other test_run_* methods rely
        on this, if this ever changes, you need to verify them as well.'''
        self.bm._run()
        cmd_calls = [call for call in self.mock_ssh.method_calls if call[0] == 'cmd']
        assert len(cmd_calls) == 1
        pos_args = cmd_calls[0][1]
        assert len(pos_args) == 1
        cmdline = pos_args[0]
        assert isinstance(cmdline, basestring)

    def test_run_escape(self):
        '''Cmdline arguments must be escaped when running the task on minion'''
        self.ref_arg_data['task'] = 'my sneaky task*; name.yaml'
        self.ref_arg_data['_orig_args']['item'] = 'foo (bar); baz'
        self.bm._run()
        cmd_calls = [call for call in self.mock_ssh.method_calls if call[0] == 'cmd']
        pos_args = cmd_calls[0][1]
        cmdline = pos_args[0]
        elems = shlex.split(cmdline)
        assert self.ref_arg_data['task'] in elems
        assert self.ref_arg_data['_orig_args']['item'] in elems

    def test_run_formula_path(self):
        '''We have to adjust formula path to local directory'''
        self.ref_arg_data['task'] = '/some/host/path/task.yaml'
        self.bm._run()
        cmd_calls = [call for call in self.mock_ssh.method_calls if call[0] == 'cmd']
        pos_args = cmd_calls[0][1]
        cmdline = pos_args[0]
        elems = shlex.split(cmdline)
        assert 'task.yaml' in elems
        assert self.ref_arg_data['task'] not in elems

    def test_run_add_local(self):
        self.bm._run()
        cmd_calls = [call for call in self.mock_ssh.method_calls if call[0] == 'cmd']
        pos_args = cmd_calls[0][1]
        cmdline = pos_args[0]
        elems = shlex.split(cmdline)
        assert '--local' in elems

    def test_run_exclude_args(self):
        self.bm.arg_data_exclude = ['item']
        self.bm._run()
        cmd_calls = [call for call in self.mock_ssh.method_calls if call[0] == 'cmd']
        pos_args = cmd_calls[0][1]
        cmdline = pos_args[0]
        elems = shlex.split(cmdline)
        assert '--item' not in elems
        assert 'foo' not in elems
        assert '--type' in elems


@pytest.mark.usefixtures('setup')
class TestRemoteMinion(object):
    '''A common class for running tests shared between all remote minions'''

    remote_minions = (minion.PersistentMinion, minion.DisposableMinion)

    # everything calls setup, and setup is parametrized, therefore everything gets run for all
    # remote minion classes
    @pytest.fixture(params=remote_minions)
    def setup(self, monkeypatch, request):
        minion_cls = request.param
        self.ref_arg_data = {'artifactsdir': '/somedir/',
                             'task': '/path/mytask.yaml',
                             'ssh_privkey': None,
                             '_orig_args': {'item': 'foo',
                                            'type': 'koji_tag'},
                             'no_destroy': False,
                             'uuid': 1,
                             }

        monkeypatch.setattr(minion, 'logger', mock.Mock())
        monkeypatch.setattr(minion, 'log', mock.Mock())
        monkeypatch.setattr(minion, 'remote_exec', mock.MagicMock())
        monkeypatch.setattr(minion, 'taskformula', mock.MagicMock())
        monkeypatch.setattr(minion, 'vm', mock.Mock())

        self.mock_prepare = mock.Mock()
        self.mock_run = mock.Mock()
        self.mock_output = mock.Mock()
        self.mini = minion_cls(None, self.ref_arg_data)
        monkeypatch.setattr(self.mini, '_prepare_task', self.mock_prepare)
        monkeypatch.setattr(self.mini, '_run', self.mock_run)
        monkeypatch.setattr(self.mini, '_get_output', self.mock_output)

    def test_timeout_no_logs(self):
        '''don't gather logs if timeout is reached during execution'''
        self.mock_run.side_effect = exc.TaskotronRemoteTimeoutError()

        with pytest.raises(exc.TaskotronRemoteTimeoutError):
            self.mini.execute()

        assert self.mock_output.called is False

    def test_crash_get_logs(self):
        '''logs should be gathered after execution crash'''
        self.mock_run.side_effect = exc.TaskotronRemoteProcessError()

        with pytest.raises(exc.TaskotronRemoteProcessError):
            self.mini.execute()

        assert self.mock_output.call_count == 1

    def test_no_crash_get_logs(self):
        '''logs should be gathered if execution doesn't crash'''
        self.mini.execute()
        assert self.mock_output.call_count == 1
